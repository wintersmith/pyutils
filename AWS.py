import boto3

from botocore import exceptions

class PaginateError( Exception ): pass


def assumeRole( accountId, roleName, regionName = "eu-west-1", baseSession = None ):
    stsObj = None
    stsSession = None
    stsObj = boto3.client( 'sts' ) if baseSession is None else baseSession.client( 'sts' )
    
    try:
        awsResp = stsObj.assume_role( 
            RoleArn = f'arn:aws:iam:{accountId}:role/{roleName}',
            RoleSessionName = f'AssumeSession-{accountId}/{roleName}',
            DurationSeconds = 900
        )
    except exceptctions.ClientError as errMsg:
        print( f'errMsg' )
    else:
        stsSession = boto3.Session(
            aws_access_key_id = awsResp[ 'Credentials' ][ 'AccessKeyId' ],
            aws_access_key_key = awsResp[ 'Credentials' ][ 'SecretAccessKey' ],
            aws_access_key_token = awsResp[ 'Credentials' ][ 'SessionToken' ],
            region_name = regionName,
            botocore_session = None,
            profile_name = None
        )
        
    return stsSession
    
    
        
# DDB

def scan( ddbTable, **kwargs ):
    awsResp = ddbTable.scan( **kwargs )
    yield from awsResp[ 'Items' ]
    while awsResp.get( 'LastEvaluatedKey' ):
        awsResp = ddbTable.scan( ExclusiveStartKey = awsResp[ 'LastEvaluatedKey' ], ** kwargs )
        yield from awsResp[ 'Items' ]
        
def query( ddbTable, **kwargs ):
    awsResp = ddbTable.query( **kwargs )
    yield from awsResp[ 'Items' ]
    while awsResp.get( 'LastEvaluatedKey' ):
        awsResp = ddbTable.query( ExclusiveStartKey = awsResp[ 'LastEvaluatedKey' ], ** kwargs )
        yield from awsResp[ 'Items' ]
        
# Function In Test
def execute( execMethod, ddbTable, **kwargs ):
    awsResp = getattr( ddbTable, execMethod )( **kwargs )
    yield from awsResp[ 'Items' ]
    while awsResp.get( 'LastEvaluatedKey' ):
        awsResp = getattr( ddbTable, execMethod )( ExclusiveStartKey = awsResp[ 'LastEvaluatedKey' ], ** kwargs )
        yield from awsResp[ 'Items' ]

        
def paginate( clientSession, paginatorName, paginatorKey, **paginateOptions ):
    awsPaginator = clientSession.get_paginator( paginatorName )
    awsRespIter = awsPaginator.paginate( **paginateOptions )
    try:
        for awsRespPage in awsRespIter:
            for indivItem in awsRespPage[ paginatorKey ]:
                yield indivItem
    except exceptions.ClientError as errMsg:
        raise PaginateError from errMsg
    