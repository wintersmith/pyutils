import re

reUrlSchema = re.compile( '(.*?):(.*)' )
reCredentials = re.compile( '/([a-zA-Z0-9]*?):(.*?)@(.*)' )
reDbHost = re.compile( '(.*?):(\d*)(.*)' )


def parse( dbURL: str ):
    drvName = None
    userName = None
    passWord = None
    hostPort = None
    dbName = None
    dbOptions = None

    dbTypeMatch = reUrlSchema.search( dbURL )
    if dbTypeMatch:
        drvName = dbTypeMatch.group( 1 )
        if not drvName in [ 'sqlite', 'file' ]:
            reCredMatch = reCredentials.search( dbTypeMatch.group( 2 ) )
            if reCredMatch:
                userName = reCredMatch.group( 1 )
                passWord = reCredMatch.group( 2 )
                reHostPortMatch = reDbHost.search( reCredMatch.group( 3 ) )
                if reHostPortMatch:
                    hostPort = ( reHostPortMatch.group( 1 ), reHostPortMatch.group( 2 ) )
                    dbName = reHostPortMatch.group( 3 )
                else:
                    hostPort = reCredMatch.group( 3 ).split( '/', 1 )[ 0 ]
                    dbName = '/{}'.format( reCredMatch.group( 3 ).split( '/', 1 )[ 1 ] )
            else:
                dbName = dbTypeMatch.group( 2 )
        else:
            dbName = dbTypeMatch.group( 2 )
        try:
            dbName, dbOpts = dbName.split( '?' )
            dbOptions = [ dbOpt for dbOpt in dbOpts.split( '&' ) ]
        except ValueError:
            pass

    print( drvName, userName, passWord, hostPort, dbName, dbOptions )
