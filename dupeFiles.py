import os
import sys
import hashlib

from multiprocessing.pool import ThreadPool

def hashFile( ffPath ):
    if os.path.exists( ffPath ):
        try:
            with open( ffPath, 'rb' ) as fileH:
                fileHash = hashlib.md5()
                fileBuffer = fileH.read( 65536 )
                while len( fileBuffer ) > 0:
                    fileHash.update( fileBuffer )
                    fileBuffer = fileH.read( 65536 )

            return ( fileHash.hexdigest(), ffPath )
        except OSError:
            pass

    return ( None, None )


def walkPath( srcPath ):
    for rootDir, fDir, fFiles in os.walk( srcPath ):
        fDir[:] = [ iDir for iDir in fDir if not iDir[ 0 ] == '.' ]
        for indivFile in fFiles:
            fullFileName = os.path.join( rootDir, indivFile )

            yield fullFileName

def main( baseDir ):
    dupeFiles = {}
    tPool = ThreadPool( 1 )
    hashResults = tPool.map( hashFile, [ iFile for iFile in walkPath( baseDir ) ] )

    for iHash, iFile in hashResults:
        if iHash is None: continue

        if iHash in dupeFiles:
            dupeFiles[ iHash ].append( iFile )
        else:
            dupeFiles[ iHash ] = [ iFile ]
    for indivHash, fNames in sorted( dupeFiles.items(), key = lambda e: len( e[ 1 ] ), reverse = True ):
        print( indivHash, fNames )

if __name__ == '__main__':
    try:
        if os.path.exists( sys.argv[ 1 ] ) and os.path.isdir( sys.argv[ 1 ] ):
            sys.exit( main( sys.argv[ 1 ] ) )
        else:
            raise OSError()
    except ( IndexError, OSError ):
        print( "Blank / Invalid Directory Specified." )
