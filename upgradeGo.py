#!/usr/bin/env python
"""
    upgradeGo.py - As the name suggest, this script will upgrade the currently installed version of Go.  It assumes that it doesn't need any elevated rights in order to 
                    carry out the upgrade.  It will figure out the latest version of Go, as shown on golang.org, it will attempt to download the tar.gz archive, and then
                    install it, making a backup of what was there to start with.  It works on macOS, Windoze is unlikely.
"""
import os
import sys
import re
import tempfile
import shutil
import tarfile
import contextlib
try:
    import six.moves.urllib.request as request
except ImportError:
    import urllib.request as request
from platform import system, architecture

goURL = 'https://golang.org/dl/'
goDownloadURL = 'https://dl.google.com/go/'
goInstallPath = os.getenv( 'GOROOT', None )
if goInstallPath is None:
    raise Exception( "Go Lang Doesn't Appear To Be Installed" )

def _getGoVersion() -> None:
    
    reSrcDownload = re.compile( '<a class="download downloadBox" href="https://dl.google.com/go/(.*).src.tar.gz">' )
    
    with contextlib.closing( request.urlopen( goURL ) ) as httpResp:
        httpData = httpResp.read().decode( 'utf-8' )
        for indivLine in httpData.split( '\n' ):
            reMatch = reSrcDownload.search( indivLine )
            if reMatch:
                return reMatch.group( 1 )

    return None

def getGoVersion() -> str:
    return "%s%s.%s-%s.tar.gz" % ( goDownloadURL, _getGoVersion(), system().lower(), 'amd64' if architecture()[ 0 ] == '64bit' else '386' )

def downloadFile( fileURL: str, localFile: str ) -> bool:
    print( "Download %s To %s" % ( fileURL, localFile ) )
    fileStatus = True
    try:
        with request.urlopen( fileURL ) as httpResp, open( localFile, 'wb' ) as localFH:
            shutil.copyfileobj( httpResp, localFH )

    except AttributeError:
        try:
            with contextlib.closing( request.urlopen( fileURL ) ) as httpResp, open( localFile, 'wb' ) as localFH:
                shutil.copyfileobj( httpResp, localFH )
        except Exception as errMsg:                
            print( "Unable To Process Download Request, %s" % errMsg )
            fileStatus = False
    except Exception as errMsg:
        print( "Unable To Process Download Request, %s" % errMsg )
        fileStatus = False
                
    return fileStatus

def main() -> int:
    homeDirectory = os.path.expanduser( '~' )
    tempFile = os.path.join( os.path.join( homeDirectory, 'Downloads' ), next( tempfile._get_candidate_names() ) )
    if downloadFile( getGoVersion(), tempFile ):
        print( "Backing Up Previous Version" )
        shutil.move( goInstallPath, '%s%s' % ( goInstallPath, '-prev' ) )
        print( "Extracting New Version" )
        with tarfile.open( tempFile ) as tarFH:
            tarFH.extractall( path = os.path.split( goInstallPath )[ 0 ] )
        os.unlink( tempFile )

        return 0
    
if __name__ == '__main__':
    sys.exit( main() )